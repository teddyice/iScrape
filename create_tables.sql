DROP TABLE IF EXISTS Tagged_Profiles, Hash_Tags, Scraped_Profiles, Scrapes, Posts, Post_TPs, Post_HTs, Post_Scrapes; 
CREATE TABLE Tagged_Profiles(
	TP_NAME VARCHAR(30) NOT NULL, 
	tp_count INT NOT NULL,
	PRIMARY KEY (tp_name)
);

CREATE TABLE Hash_Tags(
	HT_TEXT VARCHAR(30) NOT NULL,
	ht_count INT NOT NULL,
	PRIMARY KEY (ht_text)
);

CREATE TABLE Scraped_Profiles(
	PRO_NAME VARCHAR(30),
	pro_followers INT,
	pro_following INT,
	PRIMARY KEY (pro_name)
);

CREATE TABLE Scrapes(
	SCR_NUM INT NOT NULL AUTO_INCREMENT,
	scr_timestamp BIGINT,
	pro_name VARCHAR(30) REFERENCES Profiles(pro_name),
	scr_followers INT,
	scr_following INT,
	PRIMARY KEY (scr_num)
);

CREATE TABLE Posts(
	POST_ID BIGINT NOT NULL,
	post_time BIGINT NOT NULL,
	post_wday VARCHAR(15) NOT NULL,
	post_hr INT NOT NULL,
	post_likes INT NOT NULL,
	post_comments INT NOT NULL,
	pro_name VARCHAR(30) NOT NULL REFERENCES Profiles(pro_name),
	PRIMARY KEY (post_id)
);
CREATE TABLE Post_Comments(
	COM_ID INT NOT NULL AUTO_INCRMENT,
	post_id BIGINT NOT NULL REFERENCES Posts(post_id),
	pro_name VARCHAR(30) NOT NULL,
	com_text VARCHAR(100) NOT NULL,
	PRIMARY KEY (com_id) 
);
CREATE TABLE Post_TPs(
	TP_NAME VARCHAR(30) NOT NULL REFERENCES Tagged_Profiles(tp_name),
	POST_ID BIGINT NOT NULL REFERENCES Posts(post_id),
	PRIMARY KEY (tp_name, post_id)
);

CREATE TABLE Post_HTs(
	HT_TEXT VARCHAR(30) NOT NULL REFERENCES Hash_Tags(ht_text),
	POST_ID BIGINT NOT NULL REFERENCES Posts(post_id),
	ht_active INT NOT NULL,
	PRIMARY KEY (ht_text, post_id)
);

CREATE TABLE Post_Scrapes(
	SCR_NUM INT NOT NULL REFERENCES Scrapes(scr_num),
	POST_ID BIGINT NOT NULL REFERENCES Posts(post_id),
	ps_likes INT, 
	ps_comments INT,
	PRIMARY KEY (scr_num, post_id)
);

