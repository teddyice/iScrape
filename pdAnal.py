import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
pd.set_option('max_columns', 50)

#this file will analyze an instagram profile scrape using pandas.


class InstaCSV:
	weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
	def startTk(self):
		root = tk.Tk()
		return root
	def startFigure(self, root):
		figure1 = plt.Figure(figsize=(20,10), dpi = 100)	
		canvas = FigureCanvasTkAgg(figure1, root)
		canvas.get_tk_widget().grid()
		toolbarFrame = tk.Frame(master=root)
		toolbarFrame.grid()
		toolbar = NavigationToolbar2TkAgg(canvas, toolbarFrame)
		return figure1
	def getFile(self, fileName):		
		df = pd.read_csv(fileName)
		return df
	#will get mean likes by week day
	def getMeanLikesByWDay(self, dataFrame, root, weekdays, figure1):
		#this will group by day of the week and average the likes
		df = dataFrame
		df2 = df[["likes", "Week Day"]]
		ax1 = figure1.add_subplot(224)
		a=df2.groupby('Week Day', as_index=True)['likes'].mean().reindex(weekdays)
		print(a)
		a.plot.bar(x="", y="Likes", rot=0, ax=ax1)
		ax1.set_title('Avg Likes by Weekday')
	#will get mean Like by Tag and plot
	def getMeanLikesByTag(self, dataFrame):
		#this will group by tag of the week and average the likes
		df = dataFrame  
		df2 = df[["likes", "tag"]]
		a=df2.groupby('tag', as_index=False)['likes'].mean().set_index('likes').sort_values('likes', ascending=False)
		print(a)
	#will get a count on the tags
	def getTagsCount(self, dataframe):
		df = dataframe
		a=df['tag'].value_counts()
		print(a)
	#will get mean amount of comments by week day and plot
	def getMeanCBD(self, dataFrame, root, weekdays, figure1):
		df = dataFrame
		df2 = df[["comments", "Week Day"]]
		ax1 = figure1.add_subplot(222)
		a= df2.groupby("Week Day", as_index=True)['comments'].mean().reindex(weekdays)
		print(a)
		b=a.plot.bar(rot = 0, ax=ax1)
		b.set_ylabel('AVG Comments')
		b.set_xlabel('')
		ax1.set_title('AVG Comments by Weekday')
	#def getModes(self, dataFrame):
		#will return modes	
	#will get likes over time and plot
	def getLTPlot(self, dataFrame, root, figure1):
		df = dataFrame
		df2 = df[["likes","timestamp"]]
		ax1 = figure1.add_subplot(221)
		a=df2.set_index("timestamp").sort_values(by=['timestamp'])
		print(a)
		b=a.plot.bar(ax=ax1)
		b.set_xticks([])
		ax1.set_title('Likes over Time')
	#will get mean likes by time !! needs to be worked on and make to show local time, PM and AM etc
	def getLikesTime(self, dataFrame, root, figure1):
		df = dataFrame
		df2 = df[["likes","Time"]]
		ax1 = figure1.add_subplot(223)
		a= df2.groupby("Time", as_index=True)['likes'].mean()
		print(a)	
		a.plot.bar(x="Time", y="likes", rot = 0, ax=ax1)
		ax1.set_title('Likes by Hour Posted')
		
if __name__=='__main__':
	obj1 = InstaCSV()
	root = obj1.startTk()
	figure1 = obj1.startFigure(root)
	csv = obj1.getFile("instaFile.csv")
	obj1.getLTPlot(csv, root, figure1)
	obj1.getLikesTime(csv, root, figure1)	
	obj1.getMeanCBD(csv, root, InstaCSV.weekdays, figure1)
	obj1.getMeanLikesByWDay(csv, root, InstaCSV.weekdays, figure1)
	root.mainloop()
	obj1.getMeanLikesByTag(csv)
	obj1.getTagsCount(csv)
	
	
