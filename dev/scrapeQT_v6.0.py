import sys

from selenium import webdriver
from bs4 import BeautifulSoup as bs
import time
import re
from urllib.request import urlopen
import json
from pandas.io.json import json_normalize
import pandas as pd, numpy as np
from pandas import DataFrame
import csv
#pyqt will be the gui here. 

from PyQt5.QtCore import QCoreApplication, Qt
from PyQt5.QtGui import QIcon, QColor, QPalette
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QPushButton, QAction, QMessageBox, QVBoxLayout, QGridLayout
from PyQt5.QtWidgets import QCalendarWidget, QFontDialog, QColorDialog, QTextEdit, QFileDialog
from PyQt5.QtWidgets import QCheckBox, QProgressBar, QComboBox, QLabel, QStyleFactory, QLineEdit, QInputDialog
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, FigureCanvas, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

import pymysql
import sqlalchemy
from statistics import mean

from sklearn import preprocessing
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

import math

from sql.connection import sqlConnection



#contents for README
#future plans include making a database with a few tables. 
#this would be a good app for a raspberry pi media server/ something that is always on. 
#This will allow a user to only scrape the new information from a profile.
#It will also allow a user to track the increase in followers in relation to posts, etc.
#Maybe it will be wise to track hashtag use in relation to followers or likes as well. 
#making this in c++ could be cool. would be a good way to understand reversing better.
# next step: design mysql database. 
#this database will store every post and a count of the likes and comments on each picture.
#in order to track things over time every scrape will store data without deleting the last entry. 
#so there will be a scrapes_of_post or similar table 	
#After getting this application working, the next step would be to get android and iOS versions.
 



#***It would also be cool to have an optional sign in or a different version of this application that enables a scraping of followers and how they were obtained. 
#***This definitely would be a different version of this app because I love the idea of just the public profile scraping. 
#then: implement it for use with above ideas 
def handle_non_numerical_data(df):
	columns = df.columns.values

	for column in columns:
		text_digit_vals = {}
		def convert_to_int(val):
			return text_digit_vals[val]

		if df[column].dtype != np.int64 and df[column].dtype != np.float64:
			column_contents = df[column].values.tolist()
			unique_elements = set(column_contents)
			x = 0
			for unique in unique_elements:
				if unique not in text_digit_vals:
					text_digit_vals[unique] = x
					x+=1

			df[column] = list(map(convert_to_int, df[column]))

	return df


class InstaScrape(QMainWindow):
		#this initializes the layout of the window. 
		#it will create two buttons and an entry field. 
		def __init__(self):



			#super will return the parent object which is the QMAINWINDOW
			super(InstaScrape,self).__init__()
			
			self.setGeometry(20,30, 400, 800)
			self.setWindowTitle("iScrape")

			self.extractAction = QAction('&Exit', self)
			self.extractAction.setShortcut('Ctrl+Q')
			self.extractAction.triggered.connect(self.quit)

			self.mainMenu = self.menuBar()
			self.fileMenu = self.mainMenu.addMenu('&File')
			self.fileMenu.addAction(self.extractAction)

			self.layout = QGridLayout()


			self.home()

		def quit(self):
			sys.exit()

		def home(self):
		# create a connection object. 	
			connection = sqlConnection()
			self.mydb = connection.mydb
			self.mycursor = self.mydb.cursor()

		#text box to enter profile to be scraped. 	
			urlTextbox = QLineEdit(self)
			urlTextbox.setPlaceholderText('Enter a Profile to Scrape')
			urlTextbox.setAlignment(Qt.AlignCenter)
			urlTextbox.setStyleSheet("QLineEdit{border-radius: 10px;}")


		
		# this button will trigger a scrape. 
			scrapeButton = QPushButton("Scrape", self)
			scrapeButton.setStyleSheet("QPushButton{border: blue; border-radius: 10px;}")
			scrapeButton.clicked.connect(lambda:self.getUrlList(urlTextbox.text()))


		#creates a drop down list to put our scraped profiles into.
			self.graphDropdown = QComboBox(self)
			self.graphDropdown.setEditable(True)
			self.graphDropdown.lineEdit().setAlignment(Qt.AlignCenter)
			self.graphDropdown.setStyleSheet("QComboBox{border-radius: 10px;}")

		#need to turn this into a function. maybe populate dropdown or something. 
		#first it will clear the dropdown then it will run the query and populate again.  
		#It should run when a scrape finishes. 
			self.populateGraphDropDown()

			

		#for right now this will show graphs. 
			graphButton = QPushButton("View Analytics", self)
			graphButton.setStyleSheet("QPushButton{border: blue; border-radius: 10px;}")
			graphButton.clicked.connect(lambda:self.getGraphFile(self.graphDropdown.currentText()))

		#here we add our widgets to the layout. 	
			self.layout.addWidget(urlTextbox,1, 1 )
			self.layout.addWidget(scrapeButton,1,2)
			self.layout.addWidget(self.graphDropdown,2,1)
			self.layout.addWidget(graphButton,2,2)

		#set central widget into the layout. 	
			widget = QWidget()
			widget.setLayout(self.layout)
			self.setCentralWidget(widget)

		#render the widgets	
			self.show()
		
		#close sql connection. 	
			self.mydb.close()

		def populateGraphDropDown(self):
			self.graphDropdown.clear()
			sql = ("""SELECT pro_name
					  FROM Scraped_Profiles""")
			self.mycursor.execute(sql,)
			self.scrapeProfiles = self.mycursor.fetchall()
			for item in self.scrapeProfiles:
				self.graphDropdown.addItem(item['pro_name'])


	
	#this runs after user pushes graphButton
	#user will decide which file to graph. Then makeGraph() will create a new window with a graph object. 
		def getGraphFile(self, proName):
		
		#create a graph object. will be a new pyqt window that contains matplot a figure with subplots. Subject to change. 	
			graph = GraphFigure()
			graph.showFigures(proName)

		#lets also create a new window for hashtag analysis? subject to change. 
						
		def timeSleep(self):
			time.sleep(2)
#getUrlList will run when user chooses to scape a profile. 
#it will use selenium to browse a page and gather URL's of posts.
#after making a list of URL's it will run populateDB()
		def getUrlList(self, url):
			connection = sqlConnection()

			self.mydb = connection.mydb
			self.mycursor = self.mydb.cursor()
			self.url = url        
		#this is going to use selenium to scroll and get all image urls and add them to a list. 
			browser = webdriver.Firefox()
			browser.get("https://instagram.com/"+url)
		  #Pagelength = browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			last_height = browser.execute_script("return document.body.scrollHeight")
			self.links=[]
		#get followers, following, and pro_name into database. Grab data from json source 
			source = browser.page_source
			data =bs(source, 'html.parser')
			body = data.find('body')
			script = body.find('script')
			raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
			json_data=json.loads(raw)
			posts =json_data['entry_data']['ProfilePage'][0]['graphql']['user']
			following = posts['edge_follow']['count']
			followers = posts['edge_followed_by']['count']
			print("Following:", following)
			print("Followers:", followers)
		#json.dumps(x, indent=4, sort_keys=True)	
			while True:
		   	# Scroll down to the bottom.
				browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			# Wait to load the page.     
				self.timeSleep()
				source = browser.page_source
				data=bs(source, 'html.parser')
				body = data.find('body')
				script = body.find_all('div', attrs={'class':'v1Nh3 kIKUG _bz0w'})
				for link in script:
					alink = link.find('a')
					print("found url", alink['href'])
					self.links.append('https://www.instagram.com'+alink['href'])
			# Calculate new scroll height and compare with last scroll height. 
				self.timeSleep()
				new_height = browser.execute_script("return document.body.scrollHeight")
				if new_height == last_height:
						break
				last_height = new_height
			self.links = list(dict.fromkeys(self.links)) #this removes duplicates
			print("Picture Amout:",len(self.links))
			browser.close()
		#lets insert pro_name, pro_followers, pro_following
		#This will insert newest information available into Scraped_Profiles
			sql = "REPLACE INTO Scraped_Profiles (pro_name, pro_followers, pro_following) VALUES (%s, %s, %s)"
			val = (self.url, followers, following)
			self.mycursor.execute(sql, val)
			print(time.time())
			self.scrapeTime = int(time.time())
		#this will keep track of every scrape
			sql = "INSERT INTO Scrapes (scr_timestamp, pro_name, scr_followers, scr_following) VALUES (%s,%s, %s, %s)"
			val = (self.scrapeTime,self.url, followers, following)
			self.mycursor.execute(sql, val)
			self.mydb.commit()
			self.populateDB()
	#populateDB() will create a csv using the list created in getUrlList() it will be self.links
	#it is passed url because it will name the csv based on the url. 
		def populateDB(self):		 
			length = len(self.links) + 1 # this will be the amount of lists in our list
			print("Scraping Picture Info...Please Wait...")
			for x in range(len(self.links)):
				i = x + 1
				try:				
					page = urlopen(self.links[x]).read()
					data=bs(page, 'html.parser')
					body = data.find('body')
					script = body.find('script')
					raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
					json_data=json.loads(raw)
					#if i ==1:
					#	instaFile = open('instaFile.txt','w')
					#	instaFile.write(json.dumps(json_data, indent=4, sort_keys=True))	
					#json.dumps(x, indent=4, sort_keys=True)
					#the zero here is to reference the first item in a list. So firt item in list of objects
					posts =json_data['entry_data']['PostPage'][0]['graphql']['shortcode_media']
					posts= json.dumps(posts, indent=4, sort_keys=True)
					posts = json.loads(posts)
				#lets do some timestamp shit
					timestamp = posts['taken_at_timestamp']
					self.pictureIndex= i
					self.pictureId= posts['id']
					self.pictureLikes= posts['edge_media_preview_like']['count']	
					self.pictureComments= posts['edge_media_preview_comment']['count']
					self.pictureTimestamp= timestamp
					print("\n")	
				#not all posts have tags need an if statment	
					if posts['edge_media_to_tagged_user']['edges'] == []:
						sql = "REPLACE INTO Post_TPs (post_id, tp_name) VALUES (%s, %s)"
						zeroTag = '0'
						val = (self.pictureId, str(zeroTag))
						self.mycursor.execute(sql, val)
						self.mydb.commit()
						print("No tag")
					else:
						tag = posts['edge_media_to_tagged_user']['edges']
						tagsList = []
						tagCount = 0
						for tagCount in range(len(tag)):
							tagsList.append(posts['edge_media_to_tagged_user']['edges'][tagCount]['node']['user']['username'])
							#here we check to see if this record already exists
							sql = ("""SELECT TP_NAME, POST_ID 
									    FROM Post_TPs 
									    WHERE post_id = %s 
									    AND tp_name = %s""")
							val = (int(self.pictureId), tagsList[tagCount])
							self.mycursor.execute(sql, val)
							myresult = self.mycursor.fetchall()
							#if it doesn't exist we will insert if it does exist we leave it. 
							if len(myresult) == 0 or myresult == None:
								sql = "REPLACE INTO Post_TPs (post_id, tp_name) VALUES (%s, %s)"
								val = (self.pictureId, tagsList[tagCount])
								self.mycursor.execute(sql, val)
								self.mydb.commit()
		
							#Now in we do the same with tagged Profiles.
							# If it doesnt exists it should be inserted. 
							sql = ("""SELECT tp_name
									    FROM Tagged_Profiles
									    WHERE tp_name = %s""")
							val = (tagsList[tagCount],)
							self.mycursor.execute(sql, val)
							myresult = self.mycursor.fetchall()
							#insert if it doesn't exist lets use Replace to insert it. 
							if len(myresult) == 0:
								sql = "REPLACE INTO Tagged_Profiles(TP_NAME, tp_count) VALUES (%s, %s)"
								val = (tagsList[tagCount],1)
								self.mycursor.execute(sql, val) 			
								self.mydb.commit()
							#if it exists then make sure to update with the newest count, 
							#Should we do if needed only?*Is doing an update more expensive that a read everytime?
							#the insert into Post_TPs happend above.
							else:
								sql = ("""SELECT COUNT(tp_name)
										    FROM Post_TPs
										    WHERE tp_name = %s""")
								val = (tagsList[tagCount],)
								self.mycursor.execute(sql, val)
								myresult= self.mycursor.fetchone()
								for tpCount in myresult:
									tpCount = myresult[tpCount]
									sql = ("""SELECT tp_count
												 FROM Tagged_Profiles
												 WHERE tp_name = %s
											 """)
									val = (tagsList[tagCount],)
									self.mycursor.execute(sql, val)
									myresult = self.mycursor.fetchone()
									for newVal in myresult:
										oldCount = newVal
									#if oldcount is different than new count lets update. 
									if tpCount != oldCount:
										sql = "UPDATE Tagged_Profiles SET tp_count = %s WHERE tp_name = %s"
										val = (tpCount, tagsList[tagCount])
										self.mycursor.execute(sql, val)
										self.mydb.commit()
									else:
										print("No Update Neccesary!")
						#into matrix can only hold single value	
					hour = time.strftime("%H", time.localtime(timestamp))
					self.pictureHour = hour
					day = time.strftime("%A", time.localtime(timestamp))
					self.pictureDay = day
					idToInt = int(self.pictureId)
					hourToInt = int(hour)
					#replace will be used to give Posts up to date information. 
					sql="REPLACE INTO Posts(POST_ID, post_time, post_wday, post_hr, post_likes, post_comments, pro_name) VALUES(%s, %s, %s, %s, %s, %s, %s)"
					val = (idToInt, timestamp, day, hourToInt, self.pictureLikes, self.pictureComments, self.url)
					self.mycursor.execute(sql, val)
					self.mydb.commit()
					#now lets go into Post_Scrapes.
					#SCR_NUM comes from the Scrapes Table, we still have access to timestamp. 
					#First: lets search for this record and then grab the resulting scr_num
					#i dont forsee having any records of the same timestamp? Is this an issue? lets do pro_name along with it. 
					#NO reason to scan the same profile multiple times at once right? 
					sql=("""SELECT scr_num 
							  FROM Scrapes
							  WHERE scr_timestamp = %s
							  AND pro_name = %s""")
					val= (self.scrapeTime, self.url)
					self.mycursor.execute(sql, val)
					myresult= self.mycursor.fetchone()
					if myresult != None:
						for tpCount in myresult:
							tpCount = myresult[tpCount]
							sql = "REPLACE INTO Post_Scrapes (SCR_NUM, POST_ID, ps_likes, ps_comments) VALUES (%s, %s, %s, %s)"
							val = (tpCount, idToInt, self.pictureLikes, self.pictureComments)
							self.mycursor.execute(sql, val)
					#now what do we need?Hash Tags of every post. 
					#***************************
					#if post has no caption then we cant look for it. 
					#****************************
					if posts['edge_media_to_caption']['edges'] != []:
						hashTxt = posts['edge_media_to_caption']['edges'][0]['node']['text']
						currentHashTags = re.findall(r"#(\w+)", hashTxt)	#will return a list containing all matches
						x=0
						for i in currentHashTags:
							currentHashTags[x] = str(i)
							x = x+1

					else:
						currentHashTags = []
						sql = "REPLACE INTO Post_HTs (ht_text, post_id, ht_active) VALUES (%s, %s, %s)"
						zeroTag = "0"
						val = (zeroTag, self.pictureId, 0)
						self.mycursor.execute(sql, val)
						self.mydb.commit()

					#if post has no hashtags then lets stop now.
					if not len(currentHashTags) == 0:

					#so now lets see what hash tags we've had in the past. 
					#lets do thist first that might be wise. We just went through and added any new ones.  		
					#here we grab all ht_text and status of existing hts for this post_id
						sql =("""SELECT ht_text, ht_active
									FROM Post_HTs
									WHERE post_id = %s""")
						val = (self.pictureId,)	
						self.mycursor.execute(sql,val)
						myHashResult = self.mycursor.fetchall()
						#lets grab the old hash tags out of the tuple
						if len(myHashResult) != 0:
							oldHashTags = []	
							for results in myHashResult:
								oldHashTags.append(results['ht_text'])
						elif len(myHashResult) == 0:
							oldHashTags = []
					#lets iterate through out past hash tags and see if any are no longer on the post. If they arent lets mark them inactive.
					#after we mark inactive we want to update the count.
					#Now we have the tags we want to insert them into Post_HTs
					#These shouldnt really be replicated? do we want to check for duplicates?
					#lets check	
						for hashTag in currentHashTags:
							#now if a current hash tag is not in the old ones then it is new! lets insert and update the count. 
							if hashTag not in oldHashTags:
								sql = "REPLACE INTO Post_HTs (ht_text, post_id, ht_active) VALUES (%s, %s, %s)"
								hashTag = hashTag.encode()
								print(hashTag)
								val = (hashTag, self.pictureId, 1)
								self.mycursor.execute(sql, val)
								self.mydb.commit()
							#lets update the count. 
							#now we can count our hashtags 
							#we only want the active ones.
								sql = ("""SELECT COUNT(ht_text)
									 		 FROM Post_HTs
											 WHERE ht_text = %s
											 AND ht_active = %s""")
								val = (hashTag, 1)
								self.mycursor.execute(sql,val)
								newCount = self.mycursor.fetchone()
								for item in newCount:
									print("newCount for", hashTag," is:", newCount[item])
									sql = "REPLACE INTO Hash_Tags (ht_text, ht_count) VALUES (%s, %s)"
									val = (hashTag, newCount[item])
									self.mycursor.execute(sql, val)
									self.mydb.commit()
						  #how about a hashtag that IS IN the old one eg NOT NEW
							elif hashTag in oldHashTags:
							#if it is there lets check to see if the status of activity if it is 1 lets leave it if it is 0 lets change to 1 
							#we have the myHashResult tuple how do we search if the record exists?we can search to see if the old status is off
							# we have the hashtag we want to see its other pair value. lets do myHashResult[hashTag]. 
							#so we will use the index z to iterate
							#what happens if there is a duplicate? hashtags can be mentioned more than once. 
							#if there is a duplicate then z here will not be a valid index. 
							#oldStatus will have to be queried from the database according to hashTag value here. 
								sql = ("""SELECT ht_active
											 FROM Post_HTs
											 WHERE ht_text = %s """)
								val = (hashTag,)
								self.mycursor.execute(sql, val)
								oldStatus = self.mycursor.fetchall()
								if oldStatus == 0:
								#now we now we need to update that status of the hashtag on this post. 
									sql = "UPDATE Post_HTs SET ht_active = 1 WHERE ht_text = %s AND post_id = %s"
									val = (hashTag, self.pictureLikes)
									self.mycursor.execute(sql,val)
									self.mydb.commit()
				#--------------------------------------------

				#if there is some comments lets scrape them
				#Populate the Post_Comments table

				#---------------------------------------------

					if posts['edge_media_to_parent_comment']['count'] != 0:
						#lets loop through the comments
						
						commentCount = posts['edge_media_to_parent_comment']['count']
						parentStartCount = len(posts['edge_media_to_parent_comment']['edges'])
						
						comment = 0
						parentCount = 0
						print("Comments Reported:", commentCount)
						while comment < parentStartCount:
							commentProfile = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['owner']['username']
							commText = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['text']
							commId = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['id']
							commId = int(commId)
							
						#lets start to input into the Post_Comments table
							sql = "REPLACE INTO Post_Comments (com_id, post_id, comm_pro_name, com_text) VALUES (%s, %s, %s,%s)"
							val = (commId, self.pictureId, commentProfile, commText)
							self.mycursor.execute(sql, val)
							self.mydb.commit()
				   
						#if there is a thread then lets loop through them and add them 
							threadCount = len(posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['edge_threaded_comments']['edges'])
											
							if int(threadCount) != 0:
								for threadComm in range(threadCount):
									commThread = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['edge_threaded_comments']['edges']
									commentProfile = commThread[threadComm]['node']['owner']['username']
									commText = commThread[threadComm]['node']['text']
									commId = commThread[threadComm]['node']['id']
									commId = int(commId)
						#lets start to input into the Post_Comments table
							
									sql = "REPLACE INTO Post_Comments (com_id, post_id, comm_pro_name, com_text) VALUES (%s, %s, %s,%s)"
									val = (commId, self.pictureId, commentProfile, commText)
									self.mycursor.execute(sql, val)
									self.mydb.commit()
								
							comment = comment + 1
							parentCount = parentCount + 1

				except pymysql.err.MySQLError as e:
					print(e)

					errfile = open(self.url+"errors.txt",'a')
					errfile.write(self.links[x]+"\n")
					errfile.close()

					print('\n ************\n WE SHOULDNT BE HERE!!!*** \n **** \n ****')
					#now lets make a csv file and write this into it.

			print("Profile Data Acquired!!")
			#might be a new profile so lets redo our dropdown
			self.populateGraphDropDown()
			#so now all the data get put in? have to check some edge cases but it seems fine for the most part. Make this into test again might be a good idea?
			#Now it is time to see what magic we can do with hash tags. Other than that time to make the graphing part more robust really. 
			#also the power here comes from the fact that subsequent scrapes will be saved. 
			#so starting today there is data from hereforhemp. 
			#so it is time to start writing some queries to get some useful data out of this. 
			self.mydb.close()



class MplCanvas(FigureCanvasQTAgg):

	def __init__(self, parent=None, width=10, height=10, dpi=100):

		figure = Figure(figsize=(width, height), dpi=dpi)
		figure.patch.set_facecolor((.2, .2, .2))
		super(MplCanvas, self).__init__(figure)


class GraphFigure(QMainWindow):		#this will run when a user chooses a file. 

		def __init__(self):
			super(GraphFigure, self).__init__()

			#this creates out root pyqt Widget that will have a qvlayout
			self.rootWidget = QWidget()
			self.layout = QVBoxLayout()
			self.rootWidget.setLayout(self.layout)

			self.setCentralWidget(self.rootWidget)

			self.connection = sqlalchemy.create_engine('mysql+pymysql://root:25r3ck4u@localhost:3306/insta_scrape')	
			self.connection.connect()


		def showFigures(self,proName):
			self.proName = str(proName)
			self.startFigure()
			self.getLTPlot()
			self.getLikesTime()	
			self.getMeanCBD()
			self.getMeanLikesByWDay()
			#self.getHashTagAvg()
			self.getFollowersGraph()
			self.getRecentPosts()
			self.createDataframe()
			self.correlationChart()
			self.MLP()
			#self.getMeanLikesByTag()
			#self.getTagsCount()
			self.show()
			#this is a static variable that will be used to order days of the week.
		weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']	
		#lets have startFigure open a new window. Inside the window we will put the graph in. 
		#this new window will be a new object. This will allow a user to open more than one window.  
		def startFigure(self):

			#new pyqt window
			self.setGeometry(10,35, 1800, 1000)
			windowName = " {} Analytics".format(self.proName)
			self.setWindowTitle(windowName)
			self.figure1 = MplCanvas(self, width=30, height=10, dpi=100)
			#this creates a dynamic MPL canvas that we will add to our layout. 
			self.addToolBar(NavigationToolbar(self.figure1, self))		
			self.layout.addWidget(self.figure1)

		#will get mean likes by week day
		def getMeanLikesByWDay(self):
			#this will group by day of the week and average the likes
			sql = "SELECT post_likes, post_time FROM Posts WHERE pro_name = %s"
			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
			days = []
			for i in df2['post_time']:
				day = time.strftime("%A", time.localtime(i))
				days.append(day)

			df2['days'] = days


			ax1 = self.figure1.figure.add_subplot(3,2,3)
			ax1.set_facecolor((.3, .3, .3))
			a=df2.groupby('days', as_index=True)['post_likes'].mean().reindex(GraphFigure.weekdays)
			print(a)
			a.plot.bar(x="", y="Likes", rot=0, ax=ax1, color='purple')
			ax1.set_title('Avg Likes by Weekday')
		#will get mean Like by Tag and plot
		def getMeanLikesByTag(self):
			#this will group by tag of the week and average the likes  
			df2 = self.df[["likes", "tag"]]
			a=df2.groupby('tag', as_index=False)['likes'].mean().set_index('likes').sort_values('likes', ascending=False)
			print(a)
		#will get a count on the tags
		def getTagsCount(self):
			a=self.df['tag'].value_counts()
			print(a)
		#will get mean amount of comments by week day and plot
		def getMeanCBD(self):

			sql = "SELECT post_comments, post_time FROM Posts WHERE pro_name = %s"
			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
			days = []
			for i in df2['post_time']:
				day = time.strftime("%A", time.localtime(i))
				days.append(day)

			df2['days'] = days
			ax2 = self.figure1.figure.add_subplot(3,2,4)
			ax2.set_facecolor((.3, .3, .3))

			a= df2.groupby("days", as_index=True)['post_comments'].mean().reindex(GraphFigure.weekdays)
			print(a)
			b=a.plot.bar(rot = 0, ax=ax2, color='purple')
			b.set_ylabel('AVG Comments')
			b.set_xlabel('')
			ax2.set_title('AVG Comments by Weekday')
		#def getModes(self, dataFrame):
		#will return modes	
		#will get likes over time and plot
		#need to access the individual bars.
		def squared_error(self,ys_orig,ys_line):
			return sum((ys_line - ys_orig) * (ys_line - ys_orig))
		def coeffOfDetermination(self, ys_orig,ys_line):
			y_mean_line = [mean(ys_orig) for y in ys_orig]
			squared_error_regr = self.squared_error(ys_orig, ys_line)
			squared_error_y_mean = self.squared_error(ys_orig, y_mean_line)
			return 1 - (squared_error_regr/squared_error_y_mean) 

		def getLTPlot(self):
			
			sql = "SELECT post_likes, post_time FROM Posts WHERE pro_name = %s"
			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))

			print("What did we get?", df2)

			ax3 = self.figure1.figure.add_subplot(3,2,5)
			ax3.set_facecolor((.3, .3, .3))


			xs = np.array(df2["post_time"], dtype=np.float64)	
			ys = np.array(df2["post_likes"], dtype=np.float64)	


			m = ( ( (mean(xs)*mean(ys) ) - mean(xs*ys) ) / 
			     ( (mean(xs) * mean(xs)) - mean(xs*xs)))
			bValue = mean(df2["post_likes"]) - m*mean(df2["post_time"])
			print(m, mean(df2["post_time"]), mean(df2["post_likes"]), bValue)
		#this creates a regression line. 
			regression_line = [(m*x)+bValue for x in xs]		

		#now lets calculate our coefficient of determination
			rsquared = self.coeffOfDetermination(ys, regression_line)
			print(" ltplot rsquared is:", rsquared)

		#plot the data and set some styling
			a=df2.set_index("post_time").sort_values(by=['post_time'])
			b=a.plot(ax=ax3)
			b.plot(xs, regression_line, color='purple',)
			b.set_xticks([])
			ax3.set_title('Likes over Time')	

	#this will get the avg likes by hour posted
		def getLikesTime(self):
			
			sql = "Select post_likes, post_time FROM Posts WHERE pro_name = %s"

			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
			times = []
			for i in df2['post_time']:
				hour = time.strftime("%H", time.localtime(i))
				times.append(hour)


			df2['Hour'] = times
			ax4 = self.figure1.figure.add_subplot(3,2,2)
			ax4.set_facecolor((.3, .3, .3))

			a= df2.groupby("Hour", as_index=True)['post_likes'].mean()
			a.plot.bar(x="Hour", y="post_likes", rot = 0, ax=ax4, color='purple')
			ax4.set_title('Likes by Hour Posted')
		def getHashTagAvg(self):

			sql = """select ht_text, SUM(post_likes), AVG(post_likes),COUNT(ht_text) 
					from Post_HTs, Posts 
					where Post_HTs.post_id = Posts.post_id 
					and pro_name = %s 
					group by ht_text order by AVG(post_likes) 
					desc limit 10"""

			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
			print(df2)
			ax5 = self.figure1.figure.add_subplot(3,2,5)

			t = pd.plotting.table(ax5, df2, loc='center')
			ax5.axis('off')

		def getFollowersGraph(self):

			sql = "SELECT scr_followers, scr_timestamp FROM Scrapes WHERE pro_name = %s"
			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))

			ax3 = self.figure1.figure.add_subplot(3,2,6)
			ax3.set_facecolor((.3, .3, .3))

			xs = np.array(df2["scr_timestamp"], dtype=np.float64)	
			ys = np.array(df2["scr_followers"], dtype=np.float64)	
		#this will get a best fit line. 
			m = ( ( (mean(xs)*mean(ys) ) - mean(xs*ys) ) / 
			     ( (mean(xs) * mean(xs)) - mean(xs*xs)))

			bValue = mean(df2["scr_followers"]) - m*mean(df2["scr_timestamp"])
			print(m, mean(df2["scr_timestamp"]), mean(df2["scr_followers"]), bValue)

			regression_line = [(m*x)+bValue for x in xs]		

			a=df2.set_index("scr_timestamp").sort_values(by=['scr_timestamp'])
			b=a.plot(ax=ax3)
			b.plot(xs, regression_line, color='purple')
			#b=a.plot.bar(ax=ax3)

			b.set_xticks([])
			ax3.set_title('Followers over Time')	


			rsquared = self.coeffOfDetermination(ys, regression_line)
			print("rsquared is:", rsquared)

	#this method will get the likes and comments for the 10 most recent posts. 		
		def getRecentPosts(self):

			sql = """Select post_likes, post_comments, post_id 
					 FROM (Select post_likes, post_comments, post_id 
					 	   FROM Posts WHERE pro_name = %s 
					 	   order by post_id desc limit 10) as ishmael
					 order by post_id ASC"""

			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
	
			ax4 = self.figure1.figure.add_subplot(3,2,1)
			ax4.set_facecolor((.3, .3, .3))
			df2.set_index("post_id").sort_values(by=['post_id'], ascending=False)

			a=df2.plot.bar(x="post_id", y="post_likes", rot = 0, ax=ax4, color='purple')
			b=df2.plot.bar(x="post_id", y="post_comments", rot = 0, ax=ax4, color='yellow')
			a.set_xticks([])
			b.set_xticks([])
			ax4.set_title('Recent Activity')
			ax4.legend()
	#this will add features to our dataframe.

	#i am currently only implementing one profile but maybe I should increase the size of the database using public info
	#really though it might be based on them so scraping a lot of profiles with the same hashtags and then having them be 
	#ran together for training might be the most accurate way of doing this. 

		def createDataframe(self):
			#sql query is working, BUT need to change the way post_TPs and post_HTs insert. They must insert 0s for no values!!
			sql = """select ANY_VALUE(Posts.post_time), ANY_VALUE(Posts.post_wday), ANY_VALUE(Posts.post_hr), ANY_VALUE(Posts.post_likes), ANY_VALUE(Posts.post_comments),COUNT(Post_HTs.ht_text)
					 from Posts, Post_HTs
					 where Posts.pro_name = %s
					 and Post_HTs.post_id = Posts.post_id 
					 group by Posts.post_time"""
			df2 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
			sql = """select ANY_VALUE(Posts.post_id), COUNT(Post_TPs.tp_name)
					 from Posts, Post_TPs
					 where Posts.pro_name = %s
					 and Post_TPs.post_id = Posts.post_id 
					 group by Posts.post_id"""
			df7 = pd.read_sql_query(sql, self.connection, params=(self.proName,))
			df2["COUNT(tp_names)"] = df7["COUNT(Post_TPs.tp_name)"]
			df3 = handle_non_numerical_data(df2)
			self.profileDF = df3
			x = np.array(df3.drop(['ANY_VALUE(Posts.post_likes)'], 1).astype(float))
			x = preprocessing.scale(x)
			y = np.array(df3['ANY_VALUE(Posts.post_likes)'])
		#add some features
			sql = "select AVG(post_likes) from Posts where pro_name = %s"
			val = (self.proName,)
			result =self.connection.execute(sql, val)
			myresult = result.fetchone()
			self.averageLikes = float(myresult['AVG(post_likes)']) 
		#lets add a feature of time since last post
			timeSinceLastCol = []
			x = 0
			for i in self.profileDF['ANY_VALUE(Posts.post_time)']:
				if x == 0:
					timeSinceLast = 0;
					timeSinceLastCol.append(timeSinceLast)
					x = x+1
				else:
					timeSinceLast = self.profileDF.iloc[x,0] - self.profileDF.iloc[x-1, 0]
					timeSinceLastCol.append(timeSinceLast)
					x = x+1	
			self.profileDF['TsL'] = timeSinceLastCol	
		#lets add a feature of likes over mean
			likesOverMeanCol = []
			for i in self.profileDF['ANY_VALUE(Posts.post_likes)']:
				likesOverMean = i/self.averageLikes
				likesOverMeanCol.append(float(likesOverMean))
			self.profileDF['LoM'] = likesOverMeanCol
			#clf = KMeans(n_clusters=4)
			#clf.fit(x)
			#correct = 0
			#for i in range(len(x)):
			#	predict_me = np.array(x[i].astype(float))
			#	predict_me = predict_me.reshape(-1, len(predict_me))
			#	prediction = clf.predict(predict_me)
			#	if prediction[0] == y[i]:
			#		correct += 1
			#print(correct/len(x))
			#print(df2)
		#some of this should go into create dataframe and this should only create the correlation. 	
		def correlationChart(self):
			df2 = self.profileDF
			corrMatrix = df2.corr()
			print(corrMatrix)
	#aiming for a more sophisticated regression		
		def MLP(self):
			df2 = self.profileDF
			df2 = df2.drop(columns=["ANY_VALUE(Posts.post_comments)"])
			df2.dropna(inplace=True)
			print(df2)
			scaler = preprocessing.StandardScaler()
		#here I need to split the data into training data and test data. 
			targetColumn = ["ANY_VALUE(Posts.post_likes)"]
			predictors = list(set(list(df2.columns))-set(targetColumn))
			df2[predictors] = df2[predictors]/df2[predictors].max()
			x = df2[predictors].values.astype(np.float)
			y = df2[targetColumn].values.astype(np.float)

			xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.20, random_state=30)

			
			print("length ytrain", len(ytrain))
			scaler.fit(xtrain)
			scaler.fit(xtest)

			scaledx = scaler.transform(xtrain)
			scaledxtest = scaler.transform(xtest)

			print(scaledx)
		#data is ready lets create a classifier
			firstMLP = MLPRegressor(hidden_layer_sizes=(36,36,36))
			firstMLP.fit(scaledx, ytrain)
			predict_train = firstMLP.predict(scaledx)
			predict_test = firstMLP.predict(scaledxtest)

			

			print("mean squared error with training data: ",mean_squared_error(ytrain, predict_train))
			print("RMSE training: ",math.sqrt(mean_squared_error(ytrain, predict_train)))

			print("mean squared error with testing data: ",mean_squared_error(ytest, predict_test))
			print("RMSE Testing: ",math.sqrt(mean_squared_error(ytest, predict_test)))

			dfy = DataFrame(ytrain, columns=["Actual Value"])
			dfp = DataFrame(predict_train, columns=["Predicted Value"])

			dfytest= DataFrame(ytest, columns=["Actual Value"])
			dfptest = DataFrame(predict_test, columns=["Actual Value"])

			dfy['predicted value'] = dfp

			dfytest['predicted value'] = dfptest

			print(dfy)

			print(dfytest)

#this allows one to pass commands from the command line, could put in an empty list.
def run():
	app = QApplication(sys.argv)
#going with a dark theme	  
	dark_palette = QPalette()
	dark_palette.setColor(QPalette.Window, QColor(53, 53, 53))
	dark_palette.setColor(QPalette.WindowText, Qt.white)
	dark_palette.setColor(QPalette.Base, QColor(25, 25, 25))
	dark_palette.setColor(QPalette.AlternateBase, QColor(53, 53, 53))
	dark_palette.setColor(QPalette.ToolTipBase, Qt.white)
	dark_palette.setColor(QPalette.ToolTipText, Qt.white)
	dark_palette.setColor(QPalette.Text, Qt.white)
	dark_palette.setColor(QPalette.Button, QColor(53, 53, 53))
	dark_palette.setColor(QPalette.ButtonText, Qt.white)
	dark_palette.setColor(QPalette.BrightText, Qt.red)
	dark_palette.setColor(QPalette.Link, QColor(42, 130, 218))
	dark_palette.setColor(QPalette.Highlight, QColor(42, 130, 218))
	dark_palette.setColor(QPalette.HighlightedText, Qt.black)
	app.setPalette(dark_palette)
	app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }")
	Gui = InstaScrape()
	sys.exit(app.exec_())
run()





