from bs4 import BeautifulSoup as bs
from urllib.request import urlopen
import json


page = urlopen("https://www.instagram.com/p/B-VZ7p3BUUS").read()
data=bs(page, 'html.parser')
body = data.find('body')
script = body.find('script')
raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
json_data=json.loads(raw)
instaFile = open('instaFile.txt','w')
instaFile.write(json.dumps(json_data, indent=4, sort_keys=True))	

