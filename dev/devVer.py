from selenium import webdriver
from bs4 import BeautifulSoup as bs
import time
import re
from urllib.request import urlopen
import json
from pandas.io.json import json_normalize
import pandas as pd, numpy as np
import csv
# ditching tkinter for react and a webserver. 
import tkinter as tk
from tkinter import * 
from tkinter.filedialog import askopenfilename

#going to use flask to make this into a web application. 
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
import mysql.connector
from statistics import mean


#contents for README
#future plans include making a database with a few tables. 
#this would be a good app for a raspberry pi media server/ something that is always on. 
#This will allow a user to only scrape the new information from a profile.
#It will also allow a user to track the increase in followers in relation to posts, etc.
#Maybe it will be wise to track hashtag use in relation to followers or likes as well. 
#making this in c++ could be cool. would be a good way to understand reversing better.
# next step: design mysql database. 
#this database will store every post and a count of the likes and comments on each picture.
#in order to track things over time every scrape will store data without deleting the last entry. 
#so there will be a scrapes_of_post or similar table 	
#After getting this application working, the next step would be to get android and iOS versions.
 



#***It would also be cool to have an optional sign in or a different version of this application that enables a scraping of followers and how they were obtained. 
#***This definitely would be a different version of this app because I love the idea of just the public profile scraping. 
#then: implement it for use with above ideas 
 
class InstaScrape:
		#this initializes the layout of the window. 
		#it will create two buttons and an entry field. 
		def __init__(self):
			self.mydb = mysql.connector.connect(
				host="localhost",
				user="root",
				password="25r3ck4u",
				database="insta_scrape",
				charset = "utf8mb4"
			)
			self.root= tk.Tk()
			self.root.wm_title("iScrape")
			self.root.geometry("400x100")
			self.masterFrame = tk.Frame(self.root)
			self.masterFrame.pack(side=TOP, fill=BOTH)
			frame1 = tk.Frame(self.masterFrame) 
			urlLabel = tk.Label(frame1, text="Scrape ") 
			urlLabel.pack(side=TOP) 
			urlEntry = tk.Entry(frame1, bd = 10) 
			urlEntry.pack(side=TOP) 
			urlButton = tk.Button(frame1, text="Scrape A Profile", command = lambda: self.getUrlList(urlEntry.get())) 
			urlButton.pack(side=TOP)    
			frame1.grid(row=0, column=0)
			frame2 = tk.Frame(self.masterFrame)
			csvButton = tk.Button(frame2, text = "Show Graphs", command = lambda: self.getGraphFile()) 
			csvButton.pack(side=TOP)
			frame2.grid(row=0, column=1)
			self.root.mainloop()	
		#this runs after user pushes csvButton
		#user will decide which file to graph. Then makeGraph() will create a new window with a graph object. 
		def getGraphFile(self):
			csvEntry = askopenfilename(initialdir="c:/home/root1/Desktop/python/insta",
												filetypes=(("CSV FILE", "*.csv"),),
												title="Choose a File")
			makeGraph(csvEntry)
						
		def timeSleep(self):
			time.sleep(2)
#getUrlList will run when user chooses to scape a profile. 
#it will use selenium to browse a page and gather URL's of posts.
#after making a list of URL's it will run makeCSV()
		def getUrlList(self, url):
			self.url = url        
			#this is going to use selenium to scroll and get all image urls and add them to a list. 
			browser = webdriver.Firefox()
			browser.get("https://instagram.com/"+url)
		   #Pagelength = browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			last_height = browser.execute_script("return document.body.scrollHeight")
			self.links=[]
			#get followers, following, and pro_name into database. Grab data from json source 
		 	#[edge_followed_by][count]	
			source = browser.page_source
			data =bs(source, 'html.parser')
			body = data.find('body')
			script = body.find('script')
			raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
			json_data=json.loads(raw)
			posts =json_data['entry_data']['ProfilePage'][0]['graphql']['user']
			following = posts['edge_follow']['count']
			followers = posts['edge_followed_by']['count']
			print("Following:", following)
			print("Followers:", followers)
			#json.dumps(x, indent=4, sort_keys=True)	
			while True:
		   	# Scroll down to the bottom.
				browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
				# Wait to load the page.     
				self.timeSleep()
				source = browser.page_source
				data=bs(source, 'html.parser')
				body = data.find('body')
				script = body.find_all('div', attrs={'class':'v1Nh3 kIKUG _bz0w'})
				for link in script:
					alink = link.find('a')
					print("found url", alink['href'])
					self.links.append('https://www.instagram.com'+alink['href'])
					# Calculate new scroll height and compare with last scroll height. 
				new_height = browser.execute_script("return document.body.scrollHeight")
				if new_height == last_height:
						break
				last_height = new_height
			self.links = list(dict.fromkeys(self.links)) #this removes duplicates
			print("Picture Amout:",len(self.links))
			browser.close()
			#lets insert pro_name, pro_followers, pro_following
			self.mycursor = self.mydb.cursor()
			#This will insert newest information available into Scraped_Profiles
			sql = "REPLACE INTO Scraped_Profiles (pro_name, pro_followers, pro_following) VALUES (%s, %s, %s)"
			val = (self.url, followers, following)
			self.mycursor.execute(sql, val)
			print(time.time())
			self.scrapeTime = int(time.time())
			#this will keep track of every scrape
			sql = "INSERT INTO Scrapes (scr_timestamp, pro_name, scr_followers, scr_following) VALUES (%s,%s, %s, %s)"
			val = (self.scrapeTime,self.url, followers, following)
			self.mycursor.execute(sql, val)
			self.mydb.commit()
			self.makeCSV()
#makeCSV() will create a csv using the list created in getUrlList() it will be self.links
#it is passed url because it will name the csv based on the url. 
		def makeCSV(self):		 
			length = len(self.links) + 1 # this will be the amount of lists in our list
			self.instaDict = [[0]*8 for f in range(length)] #this will be a list of lists to stuff into a csv file
			self.instaDict[0][0] = 'index'
			self.instaDict[0][1] = 'id'
			self.instaDict[0][2] = 'likes'
			self.instaDict[0][3] = 'comments'
			self.instaDict[0][4] = 'timestamp'
			self.instaDict[0][5] = 'tag' #currently is only grabbing the first tag. need to grab all tags
			self.instaDict[0][6] ='Time'
			self.instaDict[0][7] ='Week Day'
			print("Scraping Picture Info...Please Wait...")
			for x in range(len(self.links)):
				i = x + 1
				try:				
					page = urlopen(self.links[x]).read()
					data=bs(page, 'html.parser')
					body = data.find('body')
					script = body.find('script')
					raw = script.text.strip().replace('window._sharedData =', '').replace(';', '')
					json_data=json.loads(raw)
					#if i ==1:
					#	instaFile = open('instaFile.txt','w')
					#	instaFile.write(json.dumps(json_data, indent=4, sort_keys=True))	
					#json.dumps(x, indent=4, sort_keys=True)
					#the zero here is to reference the first item in a list. So firt item in list of objects
					posts =json_data['entry_data']['PostPage'][0]['graphql']['shortcode_media']
					posts= json.dumps(posts, indent=4, sort_keys=True)
					posts = json.loads(posts)
					#lets do some timestamp shit
					timestamp = posts['taken_at_timestamp']
					self.instaDict[i][0]= i
					self.instaDict[i][1]= posts['id']
					self.instaDict[i][2]= posts['edge_media_preview_like']['count']	
					self.instaDict[i][3]= posts['edge_media_preview_comment']['count']
					self.instaDict[i][4]= timestamp
					print("\n")	
					#not all posts have tags need an if statment	
					if posts['edge_media_to_tagged_user']['edges'] == []:
						self.instaDict[i][5] = 'No Tag'
					else:
						tag = posts['edge_media_to_tagged_user']['edges']
						tagsList = []
						tagCount = 0
						for tagCount in range(len(tag)):
							tagsList.append(posts['edge_media_to_tagged_user']['edges'][tagCount]['node']['user']['username'])
							#here we check to see if this record already exists
							sql = ("""SELECT TP_NAME, POST_ID 
									    FROM Post_TPs 
									    WHERE post_id = %s 
									    AND tp_name = %s""")
							val = (int(self.instaDict[i][1]), tagsList[tagCount])
							self.mycursor.execute(sql, val)
							myresult = self.mycursor.fetchall()
							#if it doesn't exist we will insert if it does exist we leave it. 
							if len(myresult) == 0 or myresult == None:
								sql = "INSERT INTO Post_TPs (post_id, tp_name) VALUES (%s, %s)"
								val = (self.instaDict[i][1], tagsList[tagCount])
								self.mycursor.execute(sql, val)
								self.mydb.commit()
		
							#Now in we do the same with tagged Profiles.
							# If it doesnt exists it should be inserted. 
							sql = ("""SELECT tp_name
									    FROM Tagged_Profiles
									    WHERE tp_name = %s""")
							val = (tagsList[tagCount],)
							self.mycursor.execute(sql, val)
							myresult = self.mycursor.fetchall()
							#insert if it doesn't exist lets use Replace to insert it. 
							if len(myresult) == 0:
								sql = "REPLACE INTO Tagged_Profiles(TP_NAME, tp_count) VALUES (%s, %s)"
								val = (tagsList[tagCount],1)
								self.mycursor.execute(sql, val) 			
								self.mydb.commit()
							#if it exists then make sure to update with the newest count, 
							#Should we do if needed only?*Is doing an update more expensive that a read everytime?
							#the insert into Post_TPs happend above.
							else:
								sql = ("""SELECT COUNT(tp_name)
										    FROM Post_TPs
										    WHERE tp_name = %s""")
								val = (tagsList[tagCount],)
								self.mycursor.execute(sql, val)
								myresult= self.mycursor.fetchone()
								for tpCount in myresult:
									sql = ("""SELECT tp_count
												 FROM Tagged_Profiles
												 WHERE tp_name = %s
											 """)
									val = (tagsList[tagCount],)
									self.mycursor.execute(sql, val)
									myresult = self.mycursor.fetchone()
									for newVal in myresult:
										oldCount = newVal
									#if oldcount is different than new count lets update. 
									if tpCount != oldCount:
										sql = "UPDATE Tagged_Profiles SET tp_count = %s WHERE tp_name = %s"
										val = (tpCount, tagsList[tagCount])
										self.mycursor.execute(sql, val)
										self.mydb.commit()
									else:
										print("No Update Neccesary!")
						#into matrix can only hold single value	
						self.instaDict[i][5] = posts['edge_media_to_tagged_user']['edges'][0]['node']['user']['username']
					hour = time.strftime("%H", time.localtime(timestamp))
					self.instaDict[i][6] = hour
					day = time.strftime("%A", time.localtime(timestamp))
					self.instaDict[i][7] = day
					idToInt = int(self.instaDict[i][1])
					hourToInt = int(hour)
					#replace will be used to give Posts up to date information. 
					sql="REPLACE INTO Posts(POST_ID, post_time, post_wday, post_hr, post_likes, post_comments, pro_name) VALUES(%s, %s, %s, %s, %s, %s, %s)"
					val = (idToInt, timestamp, day, hourToInt, self.instaDict[i][2], self.instaDict[i][3], self.url)
					self.mycursor.execute(sql, val)
					self.mydb.commit()
					#now lets go into Post_Scrapes.
					#SCR_NUM comes from the Scrapes Table, we still have access to timestamp. 
					#First: lets search for this record and then grab the resulting scr_num
					#i dont forsee having any records of the same timestamp? Is this an issue? lets do pro_name along with it. 
					#NO reason to scan the same profile multiple times at once right? 
					sql=("""SELECT scr_num 
							  FROM Scrapes
							  WHERE scr_timestamp = %s
							  AND pro_name = %s""")
					val= (self.scrapeTime, self.url)
					self.mycursor.execute(sql, val)
					myresult= self.mycursor.fetchone()
					if myresult != None:
						for tpCount in myresult:
							sql = "REPLACE INTO Post_Scrapes (SCR_NUM, POST_ID, ps_likes, ps_comments) VALUES (%s, %s, %s, %s)"
							val = (tpCount, idToInt, self.instaDict[i][2], self.instaDict[i][3])
							self.mycursor.execute(sql, val)
					#now what do we need?Hash Tags of every post. 
					#***************************
					
					#if post has no caption then we cant look for it. 
					#****************************
					if posts['edge_media_to_caption']['edges'] != []:
						hashTxt = posts['edge_media_to_caption']['edges'][0]['node']['text']
						currentHashTags = re.findall(r"#(\w+)", hashTxt)	#will return a list containing all matches
					else:
						currentHashTags = []
					#if post has no hashtags then lets stop now.
					if len(currentHashTags) == 0:
						print("NO DAMNED HASHTAGS in the caption!")
					else:
					#so now lets see what hash tags we've had in the past. 
					#lets do thist first that might be wise. We just went through and added any new ones.  		
					#here we grab all ht_text and status of existing hts for this post_id
						sql =("""SELECT ht_text, ht_active
									FROM Post_HTs
									WHERE post_id = %s""")
						val = (self.instaDict[i][1],)	
						self.mycursor.execute(sql,val)
						myHashResult = self.mycursor.fetchall()
						#lets grab the old hash tags out of the tuple
						if len(myHashResult) != 0:
							oldHashTags = []	
							for oldTuple in myHashResult:
						 		oldHashTags.append(oldTuple[0])
						elif len(myHashResult) == 0:
							oldHashTags = []
					#lets iterate through out past hash tags and see if any are no longer on the post. If they arent lets mark them inactive.
					#after we mark inactive we want to update the count.
					#Now we have the tags we want to insert them into Post_HTs
					#These shouldnt really be replicated? do we want to check for duplicates?
					#lets check	
						for hashTag in currentHashTags:
							#now if a current hash tag is not in the old ones then it is new! lets insert and update the count. 
							if hashTag not in oldHashTags:
								sql = "REPLACE INTO Post_HTs (ht_text, post_id, ht_active) VALUES (%s, %s, %s)"
								val = (hashTag, self.instaDict[i][1], 1)
								self.mycursor.execute(sql, val)
								self.mydb.commit()
							#lets update the count. 
							#now we can count our hashtags 
							#we only want the active ones.
								sql = ("""SELECT COUNT(ht_text)
									 		 FROM Post_HTs
											 WHERE ht_text = %s
											 AND ht_active = %s""")
								val = (hashTag, 1)
								self.mycursor.execute(sql,val)
								newCount = self.mycursor.fetchone()
								for item in newCount:
									print("newCount for", hashTag," is:", item)
									sql = "REPLACE INTO Hash_Tags (ht_text, ht_count) VALUES (%s, %s)"
									val = (hashTag, item)
									self.mycursor.execute(sql, val)
									self.mydb.commit()
						  #how about a hashtag that IS IN the old one eg NOT NEW
							elif hashTag in oldHashTags:
							#if it is there lets check to see if the status of activity if it is 1 lets leave it if it is 0 lets change to 1 
							#we have the myHashResult tuple how do we search if the record exists?we can search to see if the old status is off
							# we have the hashtag we want to see its other pair value. lets do myHashResult[hashTag]. 
							#so we will use the index z to iterate
							#what happens if there is a duplicate? hashtags can be mentioned more than once. 
							#if there is a duplicate then z here will not be a valid index. 
							#oldStatus will have to be queried from the database according to hashTag value here. 
								sql = ("""SELECT ht_active
											 FROM Post_HTs
											 WHERE ht_text = %s """)
								val = (hashTag,)
								self.mycursor.execute(sql, val)
								oldStatus = self.mycursor.fetchall()
								if oldStatus == 0:
								#now we now we need to update that status of the hashtag on this post. 
									sql = "UPDATE Post_HTs SET ht_active = 1 WHERE ht_text = %s AND post_id = %s"
									val = (hashTag, self.instaDict[i][2])
									self.mycursor.execute(sql,val)
									self.mydb.commit()
				#--------------------------------------------

				#if there is some comments lets scrape them
				#Populate the Post_Comments table

				#---------------------------------------------

					if posts['edge_media_to_parent_comment']['count'] != 0:
						#lets loop through the comments
						
						commentCount = posts['edge_media_to_parent_comment']['count']
						parentStartCount = len(posts['edge_media_to_parent_comment']['edges'])
						
						comment = 0
						parentCount = 0
						print("Comments Reported:", commentCount)
						while comment < parentStartCount:
							commentProfile = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['owner']['username']
							commText = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['text']
							commId = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['id']
							commId = int(commId)
							
						#lets start to input into the Post_Comments table
							sql = "REPLACE INTO Post_Comments (com_id, post_id, comm_pro_name, com_text) VALUES (%s, %s, %s,%s)"
							val = (commId, self.instaDict[i][1], commentProfile, commText)
							self.mycursor.execute(sql, val)
							self.mydb.commit()
				   
						#if there is a thread then lets loop through them and add them 
							threadCount = len(posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['edge_threaded_comments']['edges'])
											
							if int(threadCount) != 0:
								for threadComm in range(threadCount):
									commThread = posts['edge_media_to_parent_comment']['edges'][parentCount]['node']['edge_threaded_comments']['edges']
									commentProfile = commThread[threadComm]['node']['owner']['username']
									commText = commThread[threadComm]['node']['text']
									commId = commThread[threadComm]['node']['id']
									commId = int(commId)
						#lets start to input into the Post_Comments table
							
									sql = "REPLACE INTO Post_Comments (com_id, post_id, comm_pro_name, com_text) VALUES (%s, %s, %s,%s)"
									val = (commId, self.instaDict[i][1], commentProfile, commText)
									self.mycursor.execute(sql, val)
									self.mydb.commit()
								
							comment = comment + 1
							parentCount = parentCount + 1

				except mysql.connector.Error as e:
					print(e)

					errfile = open(self.url+"errors.txt",'a')
					errfile.write(self.links[x]+"\n")
					errfile.close()

					print('\n ************\n WE SHOULDNT BE HERE!!!*** \n **** \n ****')
					#now lets make a csv file and write this into it.

				with open(self.url+'.csv','w', newline="") as f:
					writer = csv.writer(f)
					writer.writerows(self.instaDict)
			print("SUCCESS, new csv file: "+self.url+".csv")
			#so now all the data get put in? have to check some edge cases but it seems fine for the most part. Make this into test again might be a good idea?
			#Now it is time to see what magic we can do with hash tags. Other than that time to make the graphing part more robust really. 
			#also the power here comes from the fact that subsequent scrapes will be saved. 
			#so starting today there is data from hereforhemp. 
			#so it is time to start writing some queries to get some useful data out of this. 
			


def makeGraph(csvFile):
	graph = GraphFigure()
	graph.showFigures(csvFile)
class GraphFigure:		#this will run when a user chooses a file. 
		def showFigures(self,csvFile):
			self.csvFile = csvFile
			self.getFile(str(csvFile))
			self.startFigure()
			self.getLTPlot()
			self.getLikesTime()	
			self.getMeanCBD()
			self.getMeanLikesByWDay()
			self.getMeanLikesByTag()
			self.getTagsCount()
			#this is a static variable that will be used to order days of the week.
		weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']	
		#lets have startFigure open a new window. Inside the window we will put the graph in. 
		#this new window will be a new object. This will allow a user to open more than one window.  
		def startFigure(self):
			self.figureRoot = tk.Tk()
			self.figureRoot.wm_title(self.csvFile+"Graph")
			self.graphFrame = tk.Frame(self.figureRoot)
			self.figure1 = plt.Figure(figsize=(15,8), dpi = 100)	
			self.canvas = FigureCanvasTkAgg(self.figure1, self.graphFrame)
			self.canvas.get_tk_widget().pack(side=BOTTOM)
			toolbarFrame = tk.Frame(self.graphFrame)
			toolbarFrame.pack(side=TOP)
			toolbar = NavigationToolbar2TkAgg(self.canvas, toolbarFrame)
			self.graphFrame.grid(row=1, column=0, rowspan = 5, columnspan=8)
		def getFile(self, fileName):		
			self.df = pd.read_csv(fileName)
		#will get mean likes by week day
		def getMeanLikesByWDay(self):
			#this will group by day of the week and average the likes
			df2 = self.df[["likes", "Week Day"]]
			ax1 = self.figure1.add_subplot(224)
			a=df2.groupby('Week Day', as_index=True)['likes'].mean().reindex(GraphFigure.weekdays)
			print(a)
			a.plot.bar(x="", y="Likes", rot=0, ax=ax1)
			ax1.set_title('Avg Likes by Weekday')
		#will get mean Like by Tag and plot
		def getMeanLikesByTag(self):
			#this will group by tag of the week and average the likes  
			df2 = self.df[["likes", "tag"]]
			a=df2.groupby('tag', as_index=False)['likes'].mean().set_index('likes').sort_values('likes', ascending=False)
			print(a)
		#will get a count on the tags
		def getTagsCount(self):
			a=self.df['tag'].value_counts()
			print(a)
		#will get mean amount of comments by week day and plot
		def getMeanCBD(self):
			df2 = self.df[["comments", "Week Day"]]
			ax2 = self.figure1.add_subplot(222)
			a= df2.groupby("Week Day", as_index=True)['comments'].mean().reindex(GraphFigure.weekdays)
			print(a)
			b=a.plot.bar(rot = 0, ax=ax2)
			b.set_ylabel('AVG Comments')
			b.set_xlabel('')
			ax2.set_title('AVG Comments by Weekday')
		#def getModes(self, dataFrame):
		#will return modes	
		#will get likes over time and plot
		#need to access the individual bars. 
		def getLTPlot(self):
			df2 = self.df[["likes","timestamp"]]
			ax3 = self.figure1.add_subplot(221)

			xs = np.array(df2["timestamp"], dtype=np.float64)	
			ys = np.array(df2["likes"], dtype=np.float64)	


			m = ( ( (mean(xs)*mean(ys) ) - mean(xs*ys) ) / 
			     ( (mean(xs) * mean(xs)) - mean(xs*xs)))

			bValue = mean(df2["likes"]) - m*mean(df2["timestamp"])
			print(m, mean(df2["timestamp"]), mean(df2["likes"]), bValue)

			regression_line = [(m*x)+bValue for x in xs]		


			a=df2.set_index("timestamp").sort_values(by=['timestamp'])
			b=a.plot(ax=ax3)
			b.plot(xs, regression_line)
			#b=a.plot.bar(ax=ax3)

			b.set_xticks([])
			ax3.set_title('Likes over Time')	


		#this will get the avg likes by hour posted


		def getLikesTime(self):
			df2 = self.df[["likes","Time"]]
			ax4 = self.figure1.add_subplot(223)
			a= df2.groupby("Time", as_index=True)['likes'].mean()
			a.plot.bar(x="Time", y="likes", rot = 0, ax=ax4)
			ax4.set_title('Likes by Hour Posted')
#this will pop up a new window that has a graph with the post information of the bar clicked. 
class ClickableBar:
		def __init__(self, bar, dataFrame):
			self.dataFrame = dataFrame
			self.rect = bar
			self.press = None
			self.background = None
 		
		def connect(self):
         #connect to the button press event, if it happesn fire onPress
			self.cidpress = self.rect.figure.canvas.mpl_connect('button_press_event', self.onPress)
		#this will pop up a new tkinter window. this window will show what?
		def makeWindow(self):
			self.figureRoot = tk.Tk()
			self.figureRoot.wm_title("Post Graph")
			self.graphFrame = tk.Frame(self.figureRoot)
			#so there is a graphFrame in the figureRoot
			#now we say how big the figure is.
			self.figure1 = plt.Figure(figsize=(15,8), dpi = 100)	
			#now there is a figure with plt (matplotlibpyplot)
			#next we use the FigueCanvasTkAgg function to put figure1 into the tkinterframe.
			#I imagine it is the graphFrame with the figure in in? now we have a canvas? 
			self.canvas = FigureCanvasTkAgg(self.figure1, self.graphFrame)
			#now we get_tk_widget() what is our widget in the canvas? 	
			self.canvas.get_tk_widget().pack(side=BOTTOM)
			#now we make a frame inside of graphFrame
			toolbarFrame = tk.Frame(self.graphFrame)
			#we put this frame along the top of graphFrame. 
			toolbarFrame.pack(side=TOP)
			#then we use this to add the toolbarFrame into the canvas. 
			toolbar = NavigationToolbar2TkAgg(self.canvas, toolbarFrame)
			#now on our canvas has this toolbar object in it. 
			self.graphFrame.grid(row=1, column=0, rowspan = 5, columnspan=8)
			#now lets get some info from dataFrame. 
			#post id is what we are after.
			#here we will create a new dataframe with a sql query.  
			post_id =self.dataFrame[['timestamp']]

		def onPress(self, buttonPressEvent):
        	#on button press we will see if the mouse is over us and pop up a new window with a graph.So we will maka a new tkinter
			#I am thinking that the new window can be part of this object?
			#is there something wrong with that?
			#if its not on our bar then nothing. 	
			if event.inaxes != self.rect.axes: return
			self.makeWindow()
	
       
obj1 = InstaScrape()

